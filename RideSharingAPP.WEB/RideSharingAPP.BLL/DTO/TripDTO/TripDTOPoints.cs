﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingAPP.BLL.DTO.TripDTO
{
    public class TripDTOPoints
    {
        public string OriginCoordinates { get; set; }
        public string EndCoordinates { get; set; }
    }
}
