﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingAPP.BLL.DTO.AccountInformationDTO
{
    public class AccountInformationDTOChangePassword
    {
        public int AccountInformationId { get; set; }
        public string Password { get; set; }
    }
}
