﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RideSharingAPP.BLL.DTO.AccountInformationDTO
{
    public class AccountInformationDTOGetEmail
    {
        public string Email { get; set; }
    }
}
