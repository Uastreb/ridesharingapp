# RideSharing

Status: is being developed

The developed application should be a platform for joint trips, where drivers and passengers agree in advance about the trip: the driver offers free seats in his car, and passengers planning to go in the same direction book them. The passenger indicates the desired point of departure and destination, after which the service displays suitable offers from drivers. After the trip, users can leave public reviews about each other.



Application under development on Asp.Net MVC

The database is written on Ms Sql server 2017.
The app was developed using visual studio 2017.
Application written on Asp.Net MVC 5.
Using the following libraries: 

- Dapper version 2.0.4
- Automapper version 9.0.0
- EntityFraemwork version 6.2.0
- Ninject version 3.2.0
- and others

The service will include:

- Registration, authorization, authentication, password recovery
- Passenger and driver profile
- Applications for travel from passengers and drivers
- Search by request
- Commenting and rating trips

Use Case Diagramm:

![](C:\Users\Вадим\Pictures\uml.PNG)



Database Diagramm:

![](C:\Users\Вадим\Pictures\DataBase.PNG)

